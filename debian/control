Source: lua-systemd
Section: interpreters
Priority: optional
Maintainer: Sophie Brun <sophie@freexian.com>
Build-Depends: debhelper (>= 9), dh-lua, pkg-config, libsystemd-dev
Standards-Version: 3.9.8
Homepage: https://github.com/daurnimator/lua-systemd
Vcs-Git: https://salsa.debian.org/lua-team/lua-systemd.git
Vcs-Browser: https://salsa.debian.org/lua-team/lua-systemd

Package: lua-systemd
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Provides: ${lua:Provides}
XB-Lua-Versions: ${lua:Versions}
Description: Systemd bindings for Lua
 This package provides a library for working with systemd from scripts and
 daemons written in Lua.
 .
 Where necessary, the low level libsystemd functions have been bound in C.
 Higher level functions with more idiomatic Lua semantics are written in Lua on
 top of these C primitives.

Package: lua-systemd-dev
Section: libdevel
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Architecture: any
Depends: lua-systemd (= ${binary:Version}), ${misc:Depends}
Provides: ${lua:Provides}
XB-Lua-Versions: ${lua:Versions}
Description: Development files for lua-systemd library
 This package contains the development files of the lua-systemd library,
 useful to create a statically linked binary (like a C application or a
 standalone Lua interpreter).
 .
 Documentation is also shipped within this package.
